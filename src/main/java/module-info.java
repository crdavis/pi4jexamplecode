
module com.mycompany.pi4jexamplecode {
    requires com.pi4j;
    requires com.pi4j.plugin.raspberrypi;
    requires com.pi4j.plugin.pigpio;
    requires com.pi4j.library.pigpio;
    requires org.slf4j;
    requires org.slf4j.simple;
    
    uses com.pi4j.extension.Extension;
    uses com.pi4j.provider.Provider;
}
