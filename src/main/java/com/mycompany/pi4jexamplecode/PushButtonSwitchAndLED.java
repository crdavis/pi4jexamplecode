package com.mycompany.pi4jexamplecode;

import com.pi4j.Pi4J;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.DigitalOutput;
import com.pi4j.io.gpio.digital.DigitalState;
import com.pi4j.io.gpio.digital.DigitalStateChangeEvent;
import com.pi4j.io.gpio.digital.PullResistance;
import com.pi4j.util.Console;


/**
 *
 * @author Carlton Davis
 */

/* In this demo, a button is used as a controller to control
   a blinking LED. Each subsequent pressing of the button cause
   the LED to blink faster. When the button is pressed 6 times
   the application ends.
*/
public class PushButtonSwitchAndLED {
    
    private static final int PIN_BUTTON = 18; //Physical PIN 12
    private static final int PIN_LED = 17; // Physical PIN 11
    
    private static int pressCount = 0;
    
    public static void main(String[] args) throws Exception {
        
        //Create Pi4J console wrapper/helper
        final var console = new Console();
        
        
        //Initialize the Pi4J Runtime Context
        var pi4j = Pi4J.newAutoContext();
        
        //Print information about the initialized platforms and providers
        PrintInfo.printLoadedPlatforms(console, pi4j);
        
        //Configure the button
        var buttonConfig = DigitalInput.newConfigBuilder(pi4j)
                .id("button")
                .name("Press button")
                .address(PIN_BUTTON)
                .pull(PullResistance.PULL_DOWN)
                .debounce(3000L)
                .provider("pigpio-digital-input");
        
        var button = pi4j.create(buttonConfig);
        
        //Set up listener for the button
        button.addListener(e -> {
            if (e.state() == DigitalState.LOW) {
                pressCount++;
                console.println("Button press count: " + pressCount);
            }
        });
        

        //Configure the LED
        var ledConfig = DigitalOutput.newConfigBuilder(pi4j)
                .id("led")
                .name("LED Flasher")
                .address(PIN_LED)
                .shutdown(DigitalState.LOW)
                .initial(DigitalState.LOW)
                .provider("pigpio-digital-output");
        
        var led = pi4j.create(ledConfig);


        //Print info about the state of all I/O Pi4J manages.
        PrintInfo.printRegistry(console, pi4j);

        while (pressCount < 6) {
            if (led.equals(DigitalState.HIGH)) {
                console.println("LED low");
                led.low();
            } else {
                console.println("LED high");
                led.high();
            }
            //Blinks faster as press count increases
            Thread.sleep(600 / (pressCount + 1));
        }


        // Shutdown Pi4J
        pi4j.shutdown(); 
    }
}
    
