package com.mycompany.pi4jexamplecode;

import com.pi4j.context.Context;
import com.pi4j.platform.Platforms;
import com.pi4j.provider.Providers;
import com.pi4j.registry.Registry;
import com.pi4j.util.Console;

/**
 *
 * @author Carlton Davis
 */
public class PrintInfo {
     
    //Method to print information about the detected and loaded platforms.
    public static void printLoadedPlatforms(Console console, Context pi4j) {
        Platforms platforms = pi4j.platforms();

        /* Print info about the detected and loaded platform that Pi4J 
           detected when it was initialized.
        */
        console.box("Pi4J PLATFORMS");
        console.println();
        platforms.describe().print(System.out);
        console.println();
    } 
    
    //Method to print information about the detected and loaded providers.
    public static void printProviders(Console console, Context pi4j) {
        Providers providers = pi4j.providers();

        
        /* Print info about the detected and loaded providers that Pi4J 
           detected when it was initialized.
        */
        console.box("Pi4J PROVIDERS");
        console.println();
        providers.describe().print(System.out);
        console.println();
    }
    
    //Method to print info about the state of all I/O Pi4J manages.
    public static void printRegistry(Console console, Context pi4j) {
        Registry registry = pi4j.registry();

        /*Print info about the detected and loaded I/O interfaces
          registered with Pi4Jand included in the 'Registry'
        */
        console.box("Pi4J REGISTRY");
        console.println();
        registry.describe().print(System.out);
        console.println();
    }
    
}
